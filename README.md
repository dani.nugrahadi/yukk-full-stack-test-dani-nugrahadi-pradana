## laravel framework
using laravel framework version 10

## Installation
1. copy .env.example .env
2. composer Install
3. npm install && npm run dev
4. php artisan migrate
5. php artisan key:generate
8. php artisan serve
