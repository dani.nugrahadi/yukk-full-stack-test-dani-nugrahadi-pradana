<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['middleware' => ['auth']], function() {
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
    Route::get('users', [App\Http\Controllers\User\HistoryUserBalanceController::class, 'index']);
    Route::get('users/get-data', [App\Http\Controllers\User\HistoryUserBalanceController::class, 'getData']);
    Route::get('users/input', [App\Http\Controllers\User\HistoryUserBalanceController::class, 'input'])->name('input');
    Route::post('users/store', [App\Http\Controllers\User\HistoryUserBalanceController::class, 'store']);
    // Route::post('/upload-article', [App\Http\Controllers\Article\ArticleController::class, 'uploadImage']);

    // Route::group(['middleware' => ['role:administrator']], function () {
    //     Route::resource('users', App\Http\Controllers\User\UserController::class);
    // });
});
