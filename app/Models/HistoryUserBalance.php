<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HistoryUserBalance extends Model
{
    use HasFactory;
    protected $casts = [
        'date_transaction' => 'date',
    ];
    
    protected $guarded = [
        'id',
        'created_at',
        'updated_at'
    ];

    public function user() {
        return $this->belongsTo(User::class);
    }
}
