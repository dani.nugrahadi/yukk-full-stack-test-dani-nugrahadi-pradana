<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\{UserBalance, HistoryUserBalance};
use App\Traits\ResponseBuilder;
use App\Http\Requests\User\StoreRequest;

class HistoryUserBalanceController extends Controller
{
    use ResponseBuilder;    
    /* const PER_PAGE = 10;
    const CURRENT_PAGE = 1; */

    public function index(Request $request) {
        try {
            /* $perPage = self::PER_PAGE;
            $currentPage = $request->page ?? self::CURRENT_PAGE;

            $paging = (($perPage * $currentPage)-$perPage)+1; */

            $userBalance = UserBalance::where(['user_id' => auth()->user()->id])->first();

            return view('pages/user/index', compact('userBalance'));
        }catch(\Throwable $e) {
            // $this->report($e);

            return $this->sendError(400, 'Whoops, looks like something went wrong #index');
        }
    }

    public function getData(Request $request) {
        try{
            $dateFrom = $request->input('dateFrom');
            $dateTo = $request->input('dateTo');

            $query = HistoryUserBalance::latest('created_at');

            if ($dateFrom && $dateTo) {
                $dateFrom = date('Y-m-d', strtotime($dateFrom));
                $dateTo = date('Y-m-d', strtotime($dateTo));
                // Filter the data based on the date range
                $query->whereBetween('date_transaction', [$dateFrom, $dateTo]);
            }

            $data = $query->get();

            return datatables()->of($data)
                    ->editColumn('date_transaction', function ($row) {
                        return $row->date_transaction->format('d M Y');
                    })
                    ->editColumn('amount', function ($row) {
                        $formattedAmount = number_format($row->amount, 2, ',', '.');
                        if ($row->type === 'topup') {
                            $formattedAmount = '+ Rp. ' . $formattedAmount;
                        } else {
                            $formattedAmount = '- Rp. ' . $formattedAmount;
                        }
                        return $formattedAmount;
                    })
                    ->make(true);
        }catch(\Throwable $e) {
            // $this->report($e);

            return $this->sendError(400, $e->getMessage());
        }
    }

    public function input() {
        try {
            return view('pages/user/input');
        }catch(\Throwable $e) {
            // $this->report($e);

            return $this->sendError(400, 'Whoops, looks like something went wrong #input');
        }
    }

    public function store(Request $request){
        try{
            $userBalance = UserBalance::where(['user_id' => auth()->user()->id])->first();

            $amount = str_replace(".", "", $request->amount);

            if($userBalance) {
                if($request->type == 'transaction' && $amount > $userBalance->amount) {
                    return $this->sendResponse(null, 'Jumlah transaksi tidak bisa lebih besar dari jumlah saldo anda', 400);
                }
            }

            $item = array(
                'date_transaction' => date('Y-m-d'),
                'user_id'  => auth()->user()->id,
                'type' => $request->type,
                'amount' => $amount,
                'note' => $request->note,
            );
            $user = HistoryUserBalance::create($item);

            if($user) {
                $userBalance = UserBalance::where(['user_id' => auth()->user()->id])->first();

                if($userBalance) {
                    if($request->type == 'topup') {
                        $userBalance->amount += $amount;
                    } else {
                        $userBalance->amount -= $amount;
                    }
                    $userBalance->save();
                } else {
                    UserBalance::create([
                        'user_id' => auth()->user()->id,
                        'amount' => $amount,
                    ]);
                }                

                return $this->sendResponse(null, 'Successfully insert data', 201);                
            } else {
                return $this->sendResponse(null, 'Failed to insert data', 400);
            }

		}catch(\Throwable $e) {
            // $this->report($e);

            return $this->sendError(400, 'Whoops, looks like something went wrong #store');
        }
	}
}
