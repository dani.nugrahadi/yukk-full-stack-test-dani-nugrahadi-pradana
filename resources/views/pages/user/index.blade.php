@extends('adminlte::page')

@section('title', 'Riwayat Saldo')

@section('content_header')
    <h1>Riwayat Saldo</h1>
@stop

@section('content')
<div class="col-lg-12">
    <div class="card">
        <div class="card-title">
            <div class="balance-box bg-primary float-right">
                <label class="balance-label">Saldo saat ini :</label>
                <span class="balance-amount">Rp. {{ number_format($userBalance->amount, 2, ',', '.') }}</span>
            </div>
        </div>
        <div class="card-body">
            <div class="row">
                <form class="form-inline">
                    <div class="form-group mb-2">
                        <label for="dateFrom" class="sr-only">Dari Tanggal</label>
                        <input type="text" class="form-control datepicker" value="<?php date('d M Y') ?>" data-provide="datepicker" id="dateFrom" placeholder="Dari Tanggal">
                    </div>
                    <div class="form-group mx-sm-3 mb-2">
                        <label for="inputPassword2" class="sr-only">Sampai Tanggal</label>
                        <input type="text" class="form-control datepicker" value="<?php date('d M Y') ?>" data-provide="datepicker" id="dateTo" placeholder="Sampai Tanggal">
                    </div>
                    <button id="filterButton" type="button" class="btn btn-primary mb-2" onclick="loadList()">Cari</button>
                </form>
            </div>
            <div class="table-responsive mt-4">
                <table id="history_user_balance" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Tanggal</th>
                            <th>Tipe</th>
                            <th>Jumlah</th>
                            <th>Keterangan</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
@stop

@section('css')
<style>
    .balance-box {
        display: inline-block;
        padding: 10px;
        border-radius: 5px;
        color: white;
        margin-right:20px;
        margin-top:20px;
    }

    .bg-primary {
        background-color: #007BFF; /* You can change this color to any other color you prefer */
    }

    .balance-label {
        font-weight: bold;
        margin-right: 5px;
    }

    .balance-amount {
        font-size: 24px; /* Adjust the font size as needed */
    }

</style>
@stop

@section('js')
<script type="text/javascript">
    $(".select2").select2();

    $(document).ready(function(){
        $('.datepicker').datepicker({
            format: 'dd M yyyy', // Format as needed
            todayHighlight: true,
            autoclose: true,
            orientation: 'bottom', // Set the placement to 'bottom'
        });

        $('#dateFrom').datepicker('setDate', new Date()); // Set dateFrom to today
        $('#dateTo').datepicker('setDate', new Date()); // Set dateTo to today

        $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
        });

        loadList();
    });


    function loadList() {
        const page_url = '{{ url('users/get-data') }}';
        
        var dateFrom = $('#dateFrom').val();
        var dateTo = $('#dateTo').val();

        $.fn.dataTable.ext.errMode = 'ignore';
        var table = $('#history_user_balance').DataTable({
            processing: true,
            serverSide: true,
            "bDestroy": true,
            ajax: {
                url: page_url,
                data: { dateFrom: dateFrom, dateTo: dateTo }, 
            },
            columns: [
                {data: 'date_transaction', name: 'date_transaction'},
                {data: 'type', name: 'type'},
                {data: 'amount', name: 'amount'},
                {data: 'note', name: 'note'},
            ],
            responsive: true,
            oLanguage: {
                sLengthMenu: "_MENU_",
                sSearch: ""
            },
            aLengthMenu: [[4, 10, 15, 20], [4, 10, 15, 20]],
            order: [[1, "asc"]],
            pageLength: 10,
            buttons: [
            ],
            initComplete: function (settings, json) {
                $(".dt-buttons .btn").removeClass("btn-secondary")
            },
            drawCallback: function (settings) {
                console.log(settings.json);
            }
        });

    }

    function numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    }
</script>
@stop
