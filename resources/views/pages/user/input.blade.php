@extends('adminlte::page')

@section('title', 'Form Transaksi')

@section('content_header')
    <h1>Form Transaksi</h1>
@stop

@section('content')
<div class="col-lg-12">
    <div class="card">
        <div class="card-body">
            <form id="historyBalanceForm">
                <div class="form-group">
                    <label for="type">Tipe</label>
                    <select class="form-control" id="type" name="type">
                        <option value="topup">Top Up</option>
                        <option value="transaction">Transaksi</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="amount">Jumlah</label>
                    <input type="text" class="form-control" id="amount" name="amount" oninput="formatNumberWithCommas(this)" required>
                </div>
                <div class="form-group">
                    <label for="note">Keterangan</label>
                    <textarea class="form-control" id="note" name="note" rows="3"></textarea>
                </div>
                <button type="button" id="submitBtn" class="btn btn-primary" onclick="save()">Save</button>
            </form>
        </div>
    </div>
</div>
@stop

@section('css')
@stop

@section('js')
<script type="text/javascript">
    $(".select2").select2();

    $(document).ready(function(){
        $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
        });
        clearForm()
    });

    function save()
    {
        document.getElementById("submitBtn").disabled = true;

        $.ajax({
            type: "POST",
            url: '{{ url('users/store') }}',
            beforeSend: function (xhr) {
                var token = $('meta[name="csrf_token"]').attr('content');

                if (token) {
                    return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                }
            },
            data: $('#historyBalanceForm').serialize(),
            dataType: "json",
            contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
            cache: false,
            processData: true,
            success: function(result){
                document.getElementById("submitBtn").disabled = false;

                // $('#InputModal').modal('hide');
                Swal.fire("Success!", result.message, "success").then((value) => {
                    if (value) {
                        window.location = "{{ url('users') }}";
                    }
                });
            } ,error: function(xhr, status, error) {
                Swal.fire({
                title: 'Error!',
                icon: 'error',
                html: xhr.responseJSON.message,
                });
                document.getElementById("submitBtn").disabled = false;
            },

        });
    }

    function formatNumberWithCommas(input) {
        // Remove non-numeric characters
        input.value = input.value.replace(/\D/g, '');

        // Format the number with commas
        input.value = numberWithCommas(input.value);
    }

    function numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    }

    function clearForm() {
        document.getElementById("historyBalanceForm").reset();
    }
</script>
@stop
